#include <stdio.h>
#include <stdlib.h>
#include "board.h"

void Board_init(Board *board)
{
	board->turnCnt = 0;
	// clearBoard
	for (int row=12;row--;)
		for (int col=12;col--;)
			board->board[col][row]=-1;
	for (int col=12;col--;) board->board[col][12]=0;
}


void Board_dump( Board *board)
{
	// body
	for (int row=0; row<12; row++) {
		for (int col=0; col<12; col++) {
			if (board->board[col][11-row] < 0) {
				printf("-");
			} else {
				printf("%c",Board_playerName(board->board[col][11-row]));
			}
			putchar(' ');
		}
		putchar('\n');
	}
	// footer
	for (int c=0;c<10;c++) printf("%d ",c);
	puts("A B\n");
}

int Board_setPiece( Board *board, int col)
{
	if (col < 0) return -1;
	board->board[col][board->board[col][12]]=(board->turnCnt)%2;
	board->board[col][12]++;
	board->turnCnt++;
	return 0;
}

int Board_isWin( Board *board, int x, int y, int player)
{
	int d,cx,cy;
	
	d=1;
	cx = x + 1;
	while (Board_isWin_wrap(board,cx,y)==player) { cx += 1; d += 1;}
	cx = x - 1;
	while (Board_isWin_wrap(board,cx,y)==player) { cx -= 1; d += 1;}
	if ( d>=4 ) return 1;
	
	d=1;
	cy = y - 1;
	while (Board_isWin_wrap(board,x,cy)==player) { cy -= 1; d += 1;}
	if ( d>=4 ) return 1;

	d=1;
	cx = x + 1; cy = y + 1;
	while (Board_isWin_wrap(board,cx,cy)==player) { cx += 1; cy += 1; d += 1;}
	cx = x - 1; cy = y - 1;
	while (Board_isWin_wrap(board,cx,cy)==player) { cx -= 1; cy -= 1; d += 1;}
	if ( d>=4 ) return 1;
	
	d=1;
	cx = x - 1; cy = y + 1;
	while (Board_isWin_wrap(board,cx,cy)==player) { cx -= 1; cy += 1; d += 1;}
	cx = x + 1; cy = y - 1;
	while (Board_isWin_wrap(board,cx,cy)==player) { cx += 1; cy -= 1; d += 1;}
	if ( d>=4 ) return 1;
	
	return 0;
}

int Board_isWin_wrap( Board *board, int x, int y)
{
	if (((x>=0) && (x<=11)) && ((y>=0) && (y<=11))) return board->board[x][y];
	return -1;
}

char Board_playerName(int p)
{
	if (p==0) return (char)'O';
	if (p==1) return (char)'X';
}

int Board_toAddr(char p)
{
	char s[2];
	sprintf(s,"%c",p);
	if ((p >= '0') && (p <= '9')) return atoi(s);
	else if ((p == 'a') || (p == 'A')) return 10;
	else if ((p == 'b') || (p == 'B')) return 11;
	else return -1;
}
