#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <dlfcn.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include "board.h"
#include "mod_ai/search.h"

int main ( int argc, char *argv[])
{
	Board board;
	char tmp[64],libname[2][1024];
	char a;
	int xAddr;
	int cpLv[2];
	int turnLimit;
	void *libHandle[2];
	int (*funcp[2])( Board *board, int CPLevel, int CP);
	const char *err;
	
	turnLimit = 144;
	strcpy(libname[0],"");
	cpLv[0] = 4;
	strcpy(libname[1],"");
	cpLv[1] = 4;

	int opt;
	while ( (opt = getopt( argc, argv, "x:X:o:O:n:")) != -1) {
		switch(opt) {
		case 'n':
			strcpy(tmp, optarg);
			turnLimit = atoi(tmp);
			break;
		case 'x':
			strcpy(tmp, optarg);
			cpLv[1] = atoi(tmp);
			break;
		case 'X':
			strcpy(libname[1], optarg);
			break;
		case 'o':
			strcpy(tmp, optarg);
			cpLv[0] = atoi(tmp);
			break;
		case 'O':
			strcpy(libname[0], optarg);
			break;
		case ':':
			fprintf(stderr,"%c need a value\n",opt);
			break;
		case '?':
			fprintf(stderr,"unknown option : %c\n",opt);
			break;
		}
	}

	if (libname[0]==NULL) {
		printf("search module(O) : ");
		scanf("%s",libname[0]);
	}
	libHandle[0] = dlopen(libname[0], RTLD_LAZY);
	if (libHandle[0] == NULL)
			printf("dlopen: %s\n", dlerror());
	(void) dlerror();
	*(void **) (&funcp[0]) = dlsym(libHandle[0], "search");
	err = dlerror();
	if (err != NULL)
			printf("dlsym: %s\n", dlerror());
	if (funcp == NULL)
			printf("function \"search()\" is null.\n");
	
	if (libname[1]==NULL) {
		printf("search module(X) : ");
		scanf("%s",libname[1]);
	}
	libHandle[1] = dlopen(libname[1], RTLD_LAZY);
	if (libHandle[1] == NULL)
			printf("dlopen: %s\n", dlerror());
	(void) dlerror();
	*(void **) (&funcp[1]) = dlsym(libHandle[1], "search");
	err = dlerror();
	if (err != NULL)
			printf("dlsym: %s\n", dlerror());
	if (funcp == NULL)
			printf("function \"search()\" is null.\n");


	Board_init(&board);
	srand((unsigned) time(NULL));
	printf("O.lv = %d\nX.lv = %d\n",cpLv[0],cpLv[1]);

	while ((board.turnCnt < turnLimit) || (turnLimit < 0)) {
	printf ("turn %d\n",board.turnCnt);
	Board_dump(&board);
		for (int cnt=12;cnt--;) {
			if (board.board[cnt][12] < 12) {
				break;
			}
			if (cnt==0) {
				printf("draw\n");
				return 0;
			}
		}
		if (board.turnCnt%2==0) {
			// CP1
			xAddr = (*funcp[0])( &board, cpLv[0], 0);
			Board_setPiece( &board, xAddr);
			if (Board_isWin( &board, xAddr, board.board[xAddr][12]-1, (board.turnCnt+1)%2) == 1) {
				printf ("Win: %c\n", Board_playerName((board.turnCnt+1)%2));
				break;
			}
		} else {
			// CP2
			xAddr = (*funcp[1])( &board, cpLv[1], 1);
			Board_setPiece( &board, xAddr);
			if (Board_isWin( &board, xAddr, board.board[xAddr][12]-1, (board.turnCnt+1)%2) == 1) {
				printf ("Win: %c\n", Board_playerName((board.turnCnt+1)%2));
				break;
			}
		}
	}
	Board_dump(&board);
	
	fprintf(stderr,"[INFO] finished.\n");

	dlclose(libHandle[0]);
	dlclose(libHandle[1]);

	fprintf(stderr,"[INFO] dlclosed.\n");

	return 0;
}
