#include "../board.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>

int __search_f=0;

double search_( Board *board, int maxDepth, int CP, int col)
{
	double score=(double)(rand() % 10)/10000.0;
	
	if (board->board[col][12] >= 12) return 0.0;
	board->board[col][board->board[col][12]]=(board->turnCnt)%2;
	board->turnCnt++;
	
	if (Board_isWin( board, col, board->board[col][12], (board->turnCnt+1)%2)==1) {
		board->turnCnt--;
		board->board[col][board->board[col][12]]=-1;
		__search_f=1;
		return (board->turnCnt%2==CP ? 1.0 : -1.0 );
	}
	
	board->board[col][12]++;
	
	if (maxDepth--) {
		for (int cnt=12; cnt--;) {
			score += search_( board, maxDepth, CP, cnt);
		}
	}
	board->turnCnt--;
	board->board[col][12]--;
	board->board[col][board->board[col][12]]=-1;

	return score;
}

int search ( Board *board, int maxDepth, int CP)
{
	int col=0,cnt,flag=0;
	double maxVal,val,time;
	struct {
		int column;
		double score;
	} score[12], tmp;
	
	omp_set_num_threads(omp_get_max_threads());
//	omp_set_num_threads(1);

	__search_f=0;

	maxDepth--;
	#pragma omp parallel
	{
//		time = omp_get_wtime();
		#pragma omp for
		for (cnt=11; cnt>=0; cnt--) {
			Board board_; // force private
			memcpy( board_.board, board->board, sizeof(board_.board));
			board_.turnCnt = board->turnCnt;

			score[cnt].column = cnt;
			score[cnt].score = search_( &board_, maxDepth, CP, cnt);
		}
//		printf("Timelength = %lf\n",omp_get_wtime() - time);
	}

	if(__search_f==0)
		do {
			col = rand() % 12;
		} while (board->board[col][12] >= 12);
	else {
		/* sort by score */
		for (int i=0; i<11; i++)
			for (int j=11; j>i; j--)
				if ( score[j].score < score[i].score) {
					tmp.score=score[j].score;
					tmp.column=score[j].column;
					score[j].score=score[j-1].score;
					score[j].column=score[j-1].column;
					score[j-1].score=tmp.score;
					score[j-1].column=tmp.column;
				}

		
	}
	for( cnt=12; cnt--;)
		printf("score[%d]: %d, %lf boardLen:%d\n",cnt,score[cnt].column,score[cnt].score,board->board[score[cnt].column][12]);
	for( cnt=12; cnt--;)
		if (board->board[score[cnt].column][12]<12) break;
	col = score[cnt].column;
	
//	printf("ret:%d\n",col);
	return col;
}
