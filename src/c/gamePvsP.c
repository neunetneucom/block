#include <stdio.h>
#include <stdlib.h>
#include "board.h"

int main (int argc, char **argv)
{
	Board board;
	char tmp[16];
	char a;
	int xAddr;
	int turnLimit;

	if (argc>1) {
		turnLimit = atoi(argv[1]);
	} else turnLimit = -1;
	
	Board_init(&board);
	while ((board.turnCnt < turnLimit) || (turnLimit < 0)) {
		Board_dump(&board);
		printf( "%d %c> ", board.turnCnt+1, Board_playerName(board.turnCnt%2)); // show prompt
		scanf("%s",tmp);
		a = tmp[0];
		xAddr = Board_toAddr(a);
		if (board.board[xAddr][12] >= 12) continue;
		if (Board_setPiece( &board, xAddr) == 1) continue;
		if (Board_isWin( &board, xAddr, board.board[xAddr][12]-1, (board.turnCnt+1)%2) == 1) {
			printf ("Win: %c\n", Board_playerName((board.turnCnt+1)%2));
			break;
		}
	}
	Board_dump(&board);

	return 0;
}
