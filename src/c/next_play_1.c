#include <stdio.h>
#include <stdlib.h>
#include "board.h"

int main (int argc,char **argv)
{
	Board board;
	char tmp[16];
	char a;
	int xAddr;
	int CP;
	int turnLimit;

	if (argc>1) {
		turnLimit = atoi(argv[1]);
	} else turnLimit = -1;

	do{
		printf("CP is [XO]: ");
		scanf("%s",tmp);
		switch (tmp[0]) {
			case 'X': CP=1; break;
			case 'O': CP=0; break;
			default: continue;
		}
	}while(0);

	Board_init(&board);
	while ((board.turnCnt < turnLimit) || (turnLimit < 0)) {
		Board_dump(&board);
		
		if (board.turnCnt%2==CP) {
			// CP
			printf("Suggest : ");
			int head=11,flag=0;
			for ( int cnt=11; cnt--;) {
				if (Board_isWin( &board, cnt, board.board[cnt][12], (board.turnCnt+1)%2)==1) {flag=1;if(head>cnt)head=cnt;}
			}
			for ( int cnt=11; cnt--;) {
				if (Board_isWin( &board, cnt, board.board[cnt][12], board.turnCnt%2)==1) {flag=1;if(head>cnt)head=cnt;}
			}
			if (flag==0) head=(xAddr*2873+board.turnCnt/2*1651*board.turnCnt/2)%12;
			if (head<10) {
				printf("%d\n",head);
			} else {
				printf("%c\n",head+65-10);
			}
		}
		printf( "%d %c> ", board.turnCnt+1, Board_playerName(board.turnCnt%2)); // show prompt
		scanf("%s",tmp);
		a = tmp[0];
		xAddr = Board_toAddr(a);
		if (board.board[xAddr][12] >= 12) continue;
		if (Board_setPiece( &board, xAddr) == 1) continue;
		if (Board_isWin( &board, xAddr, board.board[xAddr][12]-1, (board.turnCnt+1)%2) == 1) {
			printf ("Win: %c\n", Board_playerName((board.turnCnt+1)%2));
			break;
		}
	}
	Board_dump(&board);

	return 0;
}
