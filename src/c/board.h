typedef struct {
	int turnCnt;
	int board[12][13];
} Board;

void Board_init(Board *board);
void Board_dump( Board *board);
int Board_setPiece( Board *board, int addr);
int Board_isWin( Board *board, int x, int y, int player);
int Board_isWin_wrap( Board *board, int x, int y);
char Board_playerName(int p);
int Board_toAddr(char p);


