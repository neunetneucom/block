#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include "board.h"
#include "mod_ai/search.h"

int main (int argc,char **argv)
{
	Board board;
	char tmp[64],libname[1024];
	char a;
	int xAddr;
	int CP=-1,CPL=-1;
	int turnLimit=-1;
	void *libHandle;
	int (*funcp)( Board *board, int CPLevel, int CP);
	const char *err;
	
	int opt;
	while ( (opt = getopt( argc, argv, "l:m:p:n:")) != -1) {
		switch(opt) {
		case 'n':
			strcpy(tmp, optarg);
			turnLimit = atoi(tmp);
			break;
		case 'l':
			strcpy(tmp, optarg);
			CPL = atoi(tmp);
			break;
		case 'm':
			strcpy(libname, optarg);
			break;
		case 'p':
			strcpy(tmp, optarg);
			CP = atoi(tmp);
			break;
		case ':':
			fprintf(stderr,"%c need a value\n",opt);
			return -1;
			break;
		case '?':
			// fprintf(stderr,"unknown option : %c\n",opt);
			return -1;
			break;
		}
	}
	
	libHandle = dlopen(libname, RTLD_LAZY);
	if (libHandle == NULL) {
		printf("search module : ");
		scanf("%s",libname);
	}
	libHandle = dlopen(libname, RTLD_LAZY);
	if (libHandle == NULL) {
		printf("dlopen: %s\n", dlerror());
		return -1;
	}
	(void) dlerror();
	*(void **) (&funcp) = dlsym(libHandle, "search");
	err = dlerror();
	if (err != NULL)
		printf("dlsym: %s\n", dlerror());
	if (funcp == NULL)
		printf("function \"search()\" is null.\n");

	if (CP<0) do{
		printf("CP is [OX]: ");
		scanf("%s",tmp);
		if ((tmp[0]=='x')||(tmp[0]=='X')) {CP=1;break;}
		else if ((tmp[0]=='o')||(tmp[0]=='O')) {CP=0;break;}
	}while(1);

	if (CPL<0) CPL=4;

	if (turnLimit<0) turnLimit=144;

	Board_init(&board);
	while ((board.turnCnt < turnLimit) || (turnLimit < 0)) {
		if (board.turnCnt%2==CP) {
			xAddr = (*funcp)( &board, CPL, CP);
			Board_setPiece( &board, xAddr);
			
			if (Board_isWin( &board, xAddr, board.board[xAddr][12]-1, (board.turnCnt+1)%2) == 1) {
				printf ("Win: %c\n", Board_playerName((board.turnCnt+1)%2));
				break;
			}
		} else {
			Board_dump(&board);
			printf( "%d %c> ", board.turnCnt+1, Board_playerName(board.turnCnt%2)); // show prompt
			scanf("%s",tmp);
			a = tmp[0];
			xAddr = Board_toAddr(a);
			if (board.board[xAddr][12] >= 12) continue;
			if (Board_setPiece( &board, xAddr) == 1) continue;
			if (Board_isWin( &board, xAddr, board.board[xAddr][12]-1, (board.turnCnt+1)%2) == 1) {
				printf ("Win: %c\n", Board_playerName((board.turnCnt+1)%2));
				break;
			}
		}
	}
	Board_dump(&board);

	dlclose(libHandle);
	return 0;
}

